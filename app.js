var express = require('express');

var app = express();
app.use(express.json())


app.post('/', function(req, res) {
  res.setHeader('Content-Type', 'application/json');

  console.log(req.body);

  // handle handshake
  if (req.body.type === 'URL_VERIFICATION') {
    res.end(
      JSON.stringify({
        "challenge": req.body.challenge
      }));
  }

  // return 200
  res.end();

})

var server = app.listen(4000, function() {
  var host = server.address().address
  var port = server.address().port
  console.log("Sample app listening at http://%s:%s", host, port)
})
